// ADC.c
// Runs on LM4F120/TM4C123
// Provide functions that initialize ADC0
// Student names: Zachary Arredondo and Allison Fang
// Last modification date: 04/10/17

#include <stdint.h>
#include "tm4c123gh6pm.h"

// ADC initialization function 
// Input: none
// Output: none
void ADC_Init(void){ 
	volatile uint8_t delay;
	SYSCTL_RCGC2_R |= 0x10;
  while((SYSCTL_PRGPIO_R&0x10) != 0x10){};
  delay = SYSCTL_RCGCGPIO_R;      //   allow time for clock to stabilize
  delay = SYSCTL_RCGCGPIO_R;			
	GPIO_PORTE_DIR_R &= ~0x04;			// 2) make PE2 input
	GPIO_PORTE_AFSEL_R |= 0x04;			// 3) enable alternate function on PE2
	GPIO_PORTE_DEN_R &= ~0x04;			// 4) disable digital I/O on PE2
	GPIO_PORTE_AMSEL_R |= 0x04;     // 5) enable analog functionality on PE2
	//SYSCTL_RCGC0_R |= 0x00010000;		// 6) activate ADC0
	//delay = SYSCTL_RCGC2_R;
  
	SYSCTL_RCGCADC_R |= 0x0001;   	// 7) activate ADC0 
	//while((SYSCTL_PRADC_R&0x0001) != 0x0001){};    // good code, but not yet implemented in simulator
  delay = SYSCTL_RCGCADC_R;         // extra time for clock to stabilize
  delay = SYSCTL_RCGCADC_R;         // extra time for clock to stabilize
  delay = SYSCTL_RCGCADC_R;         // extra time for clock to stabilize
  delay = SYSCTL_RCGCADC_R;
  
	ADC0_PC_R &= ~0xF;              // 7) clear max sample rate field
  ADC0_PC_R |= 0x1;               //    configure for 125K samples/sec
  
		
	//SYSCTL_RCGC0_R &= ~0x00000300;  // 7) configure for 125K
	ADC0_SSPRI_R = 0x0123;          // 8) Sequencer 3 is highest priority
  ADC0_ACTSS_R &= ~0x0008;        // 9) disable sample sequencer 3
  ADC0_EMUX_R &= ~0xF000;         // 10) seq3 is software trigger
  ADC0_SSMUX3_R &= ~0x000F;       // 11) clear SS3 field
  ADC0_SSMUX3_R += 1;             //    set channel (channel 1 for PE2)
  ADC0_SSCTL3_R = 0x0006;         // 12) no TS0 D0, yes IE0 END0
  ADC0_IM_R &= ~0x0008;           // 13) disable SS3 interrupts
  ADC0_ACTSS_R |= 0x0008;         // 14) enable sample sequencer 3
}

//------------ADC_In------------
// Busy-wait Analog to digital conversion
// Input: none
// Output: 12-bit result of ADC conversion
uint32_t ADC_In(void){  
	uint32_t result;
	ADC0_PSSI_R |= 0x08;										// 1) initiate SS3
	while(((ADC0_RIS_R&0x08)>>3) == 0) {}		// 2) wait for conversion done
	result = ADC0_SSFIFO3_R&0x0FFF;					// 3) read result
	ADC0_ISC_R |= 0x08;											// 4) acknowledge completion
  return result; 
}
