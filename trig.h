// trig.h
#include <stdint.h>

int16_t sinrad(int16_t angle);
int16_t cosrad(int16_t angle);
int16_t acosrad(int16_t ratio);
int16_t asinrad(int16_t ratio);
