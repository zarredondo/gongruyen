// FiFo.c
// Runs on LM4F120/TM4C123
// Provide functions that implement the Software FiFo Buffer
// Last Modified: 4/16/2017 
// Student names: Zachary Arredondo and Allison Fang
// Last modification date: change this to the last modification date or look very silly

#include <stdint.h>
// --UUU-- Declare state variables for FiFo
//        size, buffer, put and get indexes
#define SIZE 16
uint32_t static PutI;
uint32_t static GetI;
int32_t static FIFO[SIZE];


// *********** FiFo_Init**********
// Initializes a software FIFO of a
// fixed size and sets up indexes for
// put and get operations
void FiFo_Init() {
// --UUU-- Complete this
	PutI = GetI = 0;
}

// *********** FiFo_Put**********
// Adds an element to the FIFO
// Input: Character to be inserted
// Output: 1 for success and 0 for failure
//         failure is when the buffer is full
uint32_t FiFo_Put(char data) {
	if(PutI - (PutI+SIZE-2)/(SIZE-1) + (SIZE - 1)*((SIZE - 1 - PutI)/(SIZE - 1)) == GetI) return 0;
	FIFO[PutI] = data;
	PutI = PutI - (PutI + SIZE-2)/(SIZE - 1) + (SIZE - 1)*((SIZE - 1 - PutI)/(SIZE - 1)); 
	return 1;	
}

// *********** FiFo_Get**********
// Gets an element from the FIFO
// Input: Pointer to a character that will get the character read from the buffer
// Output: 1 for success and 0 for failure
//         failure is when the buffer is empty
uint32_t FiFo_Get(char *datapt) {
	if(PutI == GetI) return 0;
	*datapt = FIFO[GetI];
	GetI = GetI - (GetI + SIZE - 2)/(SIZE - 1) + (SIZE - 1)*((SIZE - 1 - GetI)/(SIZE - 1));
	return 1;
}



