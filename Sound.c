// Sound.c
// This module contains the SysTick ISR that plays sound
// Runs on LM4F120 or TM4C123
// Program written by: put your names here
// Date Created: 3/6/17 
// Last Modified: 3/6/17 
// Lab number: 6
// Hardware connections
// TO STUDENTS "REMOVE THIS LINE AND SPECIFY YOUR HARDWARE********

// Code files contain the actual implemenation for public functions
// this file also contains an private functions and private data
#include <stdint.h>
#include "dac.h"
#include "tm4c123gh6pm.h"

#define A_   1420  // 880 Hz
#define C_   1194  // 1046.5 Hz
#define E1   948   // 1318.5 Hz
#define A1   710   // 1760 Hz

const uint8_t wave[64] = {
  32,35,38,41,44,47,49,52,54,56,58,
  59,61,62,62,63,63,63,62,62,61,59,
  58,56,54,52,49,47,44,41,38,35,
  32,29,26,23,20,17,15,12,10,8,
  6,5,3,2,2,1,1,1,2,2,3,
  5,6,8,10,12,15,17,20,23,26,29};

uint8_t i = 0;
	
// **************Sound_Init*********************
// Initialize Systick periodic interrupts
// Called once, with sound initially off
// Input: interrupt period
//           Units to be determined by YOU
//           Maximum to be determined by YOU
//           Minimum to be determined by YOU
// Output: none
void Sound_Init(uint32_t period){
  NVIC_ST_CTRL_R = 0;         // disable SysTick during setup
  NVIC_ST_RELOAD_R = period-1;// reload value
  NVIC_ST_CURRENT_R = 0;      // any write to current clears it
  NVIC_SYS_PRI3_R = (NVIC_SYS_PRI3_R&0x1FFFFFFF)|0x40000000; // priority 2          
  NVIC_ST_CTRL_R = 0x07; // enable SysTick with core clock and interrupts
  // enable interrupts after all initialization is finished
}

// **************Sound_Play*********************
// Start sound output, and set Systick interrupt period 
// Input: interrupt period
//           Units to be determined by YOU
//           Maximum to be determined by YOU
//           Minimum to be determined by YOU
//         input of zero disable sound output
// Output: none
void Sound_Play(uint32_t period){
	if (period != 0) {
		NVIC_ST_RELOAD_R = period-1;
	} else {
		NVIC_ST_RELOAD_R = 0;
	}
}

void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts
long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void WaitForInterrupt(void);  // low power mode
//void (*PeriodicTask)(void);   // user function

/*
void Sound_Play(const uint8_t *pt, uint32_t count){
// write this
};
*/

// Piano.c
// This software configures the off-board piano keys
// Runs on LM4F120 or TM4C123
// Program written by: put your names here
// Date Created: 3/6/17 
// Last Modified: 3/6/17 
// Lab number: 6
// Hardware connections
// TO STUDENTS "REMOVE THIS LINE AND SPECIFY YOUR HARDWARE********

// Code files contain the actual implemenation for public functions
// this file also contains an private functions and private data
#include <stdint.h>
#include "tm4c123gh6pm.h"

#define	tempo145 5517241 // 145 bpm, duration of eighth note with 16MHz clock

#define E0  3792   // 329.6 Hz
#define GF0 3378   // 370 Hz
#define G0  3189   // 392 Hz
#define A0  2841   // 440 Hz
#define B0  2531   // 493.9 Hz
#define DF_  2255   // 554.4 Hz
#define D_   2128   // 587.3 Hz
#define E_   1896   // 659.3 Hz
#define GF_  1689   // 740 Hz
#define G_   1594   // 784 Hz
#define A_	1420		// 880 Hz

const uint16_t notes_array[86] = {
	G0,B0,DF_,G0,B0,DF_,G0,B0,DF_,GF_,E_,
	DF_,D_,DF_,A0,GF0,E0,GF0,A0,GF0,G0,B0,
	DF_,G0,B0,DF_,G0,B0,DF_,GF_,E_,DF_,D_,
	GF_,D_,A0,B0,A0,E0,GF0,E0,GF0,G0,A0,B0,
	DF_,D_,DF_,GF0,G0,A0,B0,DF_,D_,E_,GF_,
	G_,A_,E0,GF0,G0,A0,B0,DF_,D_,DF_,GF0,G0,
	GF0,B0,A0,DF_,B0,D_,DF_,E_,D_,GF_,E_,G_,
	GF_,GF_,G_,E_,GF_,0};

const uint8_t rhythm_array[86] = {3,
	3,3,6,3,3,6,3,3,3,3,6,3,3,3,3,15,3,3,
	3,18,3,3,6,3,3,6,3,3,3,3,6,3,3,3,3,15,
	3,3,3,18,3,3,6,3,3,6,3,3,18,3,3,6,3,3,
	6,3,3,18,3,3,6,3,3,6,3,3,18,3,3,3,3,3,
	3,3,3,3,3,3,3,3,3,2,2,2,24};

void Timer0A_Init(void(*task)(void), uint32_t period);
void Sound_Play(uint32_t period);

uint8_t j;
uint8_t song_playing;
//uint8_t envelope;


void UserTask(void){
  Sound_Play(notes_array[j]);
	if (j == 85) {
		j = 0;
	} else {
		j++;
	}
	Timer0A_Init(&UserTask, rhythm_array[j]*tempo145);
}

void Song_Control(void) {
	if(((GPIO_PORTF_DATA_R&0x10)>>4)==0) {
		song_playing = 1;
		j = 0;
		//envelope = 1000;
		Timer0A_Init(&UserTask, rhythm_array[j]*tempo145);
	} else if ((GPIO_PORTF_DATA_R&0x1)==0) {
			song_playing = 0;
			TIMER0_CTL_R = 0x00000000;
			Sound_Play(0);
	}
}


