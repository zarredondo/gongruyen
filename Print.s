; Print.s
; Student names: Zachary Arredondo and Allison Fang
; Last modification date: change this to the last modification date or look very silly
; Runs on LM4F120 or TM4C123
; EE319K lab 7 device driver for any LCD
;
; As part of Lab 7, students need to implement these LCD_OutDec and LCD_OutFix
; This driver assumes two low-level LCD functions
; ST7735_OutChar   outputs a single 8-bit ASCII character
; ST7735_OutString outputs a null-terminated string 

STAR_ASCII		EQU	0x0000002A
DOT_OPERATOR 	EQU 0x0000002E
BOLTON			EQU 0x0000270F	; decimal value is 9999
; 2. binding step of variable DIGIT_COUNT (offset of zero)
DIGIT_COUNT 	EQU 0x00000004
; 2. binding step of variable DIVISOR
DIVISOR			EQU 0x00000004

    IMPORT   ST7735_OutChar
    IMPORT   ST7735_OutString
    EXPORT   LCD_OutDec
    EXPORT   LCD_OutFix

    AREA    |.text|, CODE, READONLY, ALIGN=2
    THUMB

  

;-----------------------LCD_OutDec-----------------------
; Output a 32-bit number in unsigned decimal format
; Input: R0 (call by value) 32-bit unsigned number
; Output: none
; Invariables: This function must not permanently modify registers R4 to R11
LCD_OutDec

	PUSH {R0, R1, R2, R3, R11, R14}
	
	; 1. allocation step of variable DIGIT_COUNT
	SUB	SP, SP, #8
	MOV	R11, SP
	
	
	MOV	R2, R0 	; Copy R0 (i.e. N) into R2 because MOD subroutine will write result to R0
	MOV R1, #10 ; 
	BL	MOD		; RO <- N % 10
	PUSH {R0, R1} ;	Push R0, i.e. N's first digit, onto the stack. R1 pushed for alignment.
	
	; 3. Access of variable DIGIT_COUNT
	; Initialize the DIGIT_COUNT to 1
	MOV R3, #1
	STR R3, [R11, #DIGIT_COUNT]
	
	; LCD_OutDec_Loop parses the digits from right to left,
	; beginning with the second digit from the right. It pushes
	; each digit onto the stack until it has exhausted all digits.
LCD_OutDec_Loop
	UDIV R2, R1	; 	N' <- N / 10
	CMP R2, #0 
	BEQ	LCD_OutDec_Print ; If N' = 0, proceed to printing block
	MOV	R0, R2 ;	R0 <- N'
	BL	MOD	; R0 <- N' % 10
	
	PUSH {R0, R1} ;	Push R0, i.e. one of N's digits, onto the stack. R1 pushed for alignment.
	; 3. Access of variable DIGIT_COUNT
	; Increment DIGIT_COUNT
	LDR	R3, [R11, #DIGIT_COUNT]
	ADD	R3, #1
	STR R3, [R11, #DIGIT_COUNT]
	
	B LCD_OutDec_Loop
	
	; LCD_OutDec_Print pops each digit from the stack one by one,
	; beginning with the right-most digit, and prints each one out.
	; it stops according to the DIGIT_COUNT variable.
LCD_OutDec_Print
	POP {R0, R1} 
	ADD	R0, #0x30
	PUSH {R0-R3}
	BL	ST7735_OutChar
	POP {R0-R3}
	
	; 3. Access of variable DIGIT_COUNT
	LDR	R3, [R11, #DIGIT_COUNT]
	SUB	R3, #1
	STR R3, [R11, #DIGIT_COUNT]
	
	CMP R3, #0
	BNE LCD_OutDec_Print
	
	; 4. deallocation of variable DIGIT_COUNT
	ADD	SP, SP, #8
	
	POP {R0, R1, R2, R3, R11, R14}

      BX  LR
;* * * * * * * * End of LCD_OutDec * * * * * * * *

; -----------------------LCD_OutFix----------------------
; Output characters to LCD display in fixed-point format
; unsigned decimal, resolution 0.001, range 0.000 to 9.999
; Inputs:  R0 is an unsigned 32-bit number
; Outputs: none
; E.g., R0=0,    then output "0.000 "
;       R0=3,    then output "0.003 "
;       R0=89,   then output "0.089 "
;       R0=123,  then output "0.123 "
;       R0=9999, then output "9.999 "
;       R0>9999, then output "*.*** "
; Invariables: This function must not permanently modify registers R4 to R11
LCD_OutFix

	PUSH{R0, LR}
	PUSH{R4-R11}
	
	; 1. allocation step for variable DIVISOR
	SUB SP, SP, #8
	MOV	R11, SP
	
	LDR	R3, =BOLTON ; R3 <- 9999
	CMP R0, R3	
	BLS	In_Range ; If N <= 9999, proceed to In_Range
	
	; Output "*.***"
	LDR R0, =STAR_ASCII
	BL	ST7735_OutChar
	LDR R0, =DOT_OPERATOR
	BL	ST7735_OutChar
	LDR R0, =STAR_ASCII
	BL	ST7735_OutChar
	LDR R0, =STAR_ASCII
	BL	ST7735_OutChar
	LDR R0, =STAR_ASCII
	BL	ST7735_OutChar
	
	B	LCD_OutFix_Done
	; In_Range block outputs the digits in the form "#.###"
	; where each # represents a digit. Unlike LCD_OutDec, the
	; number of characters is fixed at 5.
	; "(N/1000) . ((N % 1000)/100) ((N % 100)/10) (N % 10)"
In_Range
     PUSH {R0, R1, R2, R3}
	 
	 ; Output first (i.e. most significant) digit
	 
	 ; 3. Access of variable DIVISOR
	 ; Initialize the DIVISOR to 1000
	 MOV R2, #1000
	 STR R2, [R11, #DIVISOR]
	 
	 UDIV R0, R2 ; R0 <- R0 / R2 : R0 <- N / 1000
	 ADD R0, #0x30
	 BL	ST7735_OutChar
	 POP {R0, R1, R2, R3}
	 
	 
	 ; Output "."
	 PUSH {R0, R1, R2, R3}
	 LDR R0, =DOT_OPERATOR
	 BL	ST7735_OutChar
	 POP {R0, R1, R2, R3}
	 
	 
	 ; Output second digit (i.e. first digit after the decimal)
	 PUSH {R0, R1, R2, R3}
	 MOV R1, #1000 ; R1 <- 1000
	 BL	MOD ; R0 <- R0 % R1 : R0 <- N % 1000
	 
	 ; 3. Access of variable DIVISOR
	 ; Reduce DIVISOR by factor of 10 (to 100)
	 LDR R2, [R11, #DIVISOR]
	 MOV R3, #10 ; R3 <- 10
	 UDIV R2, R3 ; R2 <- R2 / R3 : DIVISOR = DIVISOR / 10
	 STR R2, [R11, #DIVISOR]
	 
	 UDIV R0, R2 ; R0 <- R0 / R2 : R0 <- (N % 1000) / 100
	 ADD R0, #0x30
	 BL	ST7735_OutChar
	 POP {R0, R1, R2, R3}
	 
	 
	 ; Output third digit (i.e. second digit after the decimal)
	 PUSH {R0, R1, R2, R3}
	 MOV R1, #100
	 BL MOD ; R0 <- R0 % R1 : R0 <- N % 100
	 
	 ; 3. Access of variable DIVISOR
	 ; Reduce DIVISOR by factor of 10 (to 10)
	 LDR R2, [R11, #DIVISOR]
	 MOV R3, #10
	 UDIV R2, R3 ; R2 <- R2 / R3 : DIVISOR = DIVISOR / 10
	 STR R2, [R11, #DIVISOR]
	 
	 UDIV R0, R2 ; R0 <- R0 / R2 : R0 <- (N % 100) / 10
	 ADD R0, #0x30
	 BL	ST7735_OutChar
	 POP {R0, R1, R2, R3}
	 
	 
	 ; Output fourth digit (i.e. third digit after the decimal)
	 PUSH {R0, R1, R2, R3}
	 MOV R1, #10 
	 BL MOD ; R0 <- R0 % R1 : R0 <- N % 10
	 ADD R0, #0x30
	 BL ST7735_OutChar
	 POP {R0, R1, R2, R3}
	 
LCD_OutFix_Done
	; 4. deallocation of variable DIVISOR
	 ADD SP, SP, #8
	 
	 POP{R4-R11}
	 POP{R0, LR}

     BX   LR
	 
;* * * * * * * * End of LCD_OutFix * * * * * * * *

; -----------------------MOD------------------------------
; inputs: R0, R1 (unsigned 32 bit numbers)
; outputs: remainder of R0 / R1 (i.e. R0 % R1)

MOD	
	PUSH{R4, R5}
	UDIV R4, R0, R1
	MUL R5, R4, R1
	SUB R0, R0, R5
	POP {R4, R5}
	BX	LR
 
     ALIGN
;* * * * * * * * End of MOD * * * * * * * *

     ALIGN                           ; make sure the end of this section is aligned
     END                             ; end of file
