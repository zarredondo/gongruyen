// SpaceInvaders.h

typedef enum{left, right, active, inactive, nopu, boltpu, vovopu, shpu, jalilisound, gongruyensound, music, shooter, normal, fall, nofall, start, play, win, lose}state;

typedef struct playerSpriteTag {
	int16_t xpos;
	int16_t ypos;
	int8_t dx;
	int8_t dy;
	uint8_t animationframes;
	uint8_t currentframe;
	uint8_t facing;
	const unsigned short *pixeldata;
	uint16_t wd;
	uint16_t ht;
	uint8_t status;
	uint8_t jumping;
	uint8_t backgroundflag;
	uint8_t powerup;
} playerSpriteType;

typedef struct enemySpriteTag {
	int16_t xpos;
	int16_t ypos;
	int8_t dx;
	int8_t dy;
	uint8_t animationframes;
	uint8_t currentframe;
	uint8_t facing;
	const unsigned short *pixeldata;
	uint16_t wd;
	uint16_t ht;
	uint8_t status;
	uint8_t jumping;
	uint8_t backgroundflag;
	uint8_t projspacetimer;
	uint8_t enemytype;
	uint8_t platform;
} enemySpriteType;

typedef struct projectileTag {
	int16_t xpos;
	int16_t ypos;
	int8_t dy;
	const unsigned short *pixeldata;
	uint8_t wd;
	uint8_t ht;
	uint8_t status;
	uint8_t backgroundflag;
} projectileType;

typedef struct platformTag {
	uint32_t xpos;
	uint32_t ypos;
	uint8_t wd;
	uint8_t ht;
	uint8_t status;
} platformType;

typedef struct powerupTag {
	int16_t xpos;
	int16_t ypos;
	const unsigned short *pixeldata;
	uint8_t wd;
	uint8_t ht;
	uint8_t status;
	uint8_t power;
	uint16_t soundlength;
	uint8_t backgroundflag;
}	powerupType; 
	


void moveEnemy(struct enemySpriteTag *point, struct platformTag *pltpoint);
void movePlayer(struct playerSpriteTag *point);
void movePlayerProjectile(struct projectileTag *point);
void moveEnemyProjectile(struct projectileTag *point);
void movePlatform(struct platformTag *point);
void movePowerUp(struct powerupTag *point);
uint8_t platformDetection(struct playerSpriteTag *point, struct platformTag *pltpoint);
void punjiDetection(struct enemySpriteTag *point, struct projectileTag *projpoint);
void punjiDetection2(struct playerSpriteTag *point, struct projectileTag *projpoint);
void powerupDetection(struct playerSpriteTag *point, struct powerupTag *pupoint);
void enemyDetection(struct playerSpriteTag *point, struct enemySpriteTag *enpoint);
void playerFireProjectile(void);
void enemyFireProjectile(void);
void cameraUpdate(void);
void gongruyenLives(void);
void titleScreen(void);
void nextLife(void);
void gameEngine(void);
void winScreen (void);
void loseScreen (void);
void enemyDies(struct enemySpriteTag *enpoint, uint8_t i);
void playerInit(int16_t xpos, int16_t ypos, int8_t dx, int8_t dy, uint8_t animationframes, uint8_t currentframe, uint8_t facing, const unsigned short *pixeldata,
	uint16_t wd, uint16_t ht, uint8_t status, uint8_t jumping, uint8_t backgroundflag, uint8_t powerup);
void enemyInit(int16_t xpos, int16_t ypos, int8_t dx, int8_t dy, uint8_t animationframes, uint8_t currentframe, uint8_t facing, const unsigned short *pixeldata,
	uint16_t wd, uint16_t ht, uint8_t status, uint8_t jumping, uint8_t backgroundflag, uint8_t projspacetimer, uint8_t enemytype, uint8_t platform, uint8_t index);
void powerupInit(int16_t xpos, int16_t ypos, const unsigned short *pixeldata, uint8_t wd,	uint8_t ht, uint8_t status, uint8_t power, uint16_t soundlength, uint8_t backgroundflag, uint8_t index);
void playerprojectileInit(int16_t xpos, int16_t ypos, int8_t dy, const unsigned short *pixeldata, uint8_t wd, uint8_t ht, uint8_t status, uint8_t backgroundflag, uint8_t index);
void enemyprojectileInit(int16_t xpos, int16_t ypos, int8_t dy, const unsigned short *pixeldata, uint8_t wd, uint8_t ht, uint8_t status, uint8_t backgroundflag, uint8_t index);
void platformInit(uint32_t xpos, uint32_t ypos, uint8_t wd, uint8_t ht, uint8_t status, uint8_t index);
void checkpointInit(uint8_t checkpointnum);
void scoreCalculateLose(void);
void scoreCalculateWin(void);
void gameInit(void);
uint8_t checkpointSet(void);

