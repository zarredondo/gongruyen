// SpaceInvaders.c
// Runs on LM4F120/TM4C123
// Jonathan Valvano and Daniel Valvano
// This is a starter project for the EE319K Lab 10

// Last Modified: 3/6/2015 
// http://www.spaceinvaders.de/
// sounds at http://www.classicgaming.cc/classics/spaceinvaders/sounds.php
// http://www.classicgaming.cc/classics/spaceinvaders/playguide.php
/* This example accompanies the books
   "Embedded Systems: Real Time Interfacing to Arm Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2014

   "Embedded Systems: Introduction to Arm Cortex M Microcontrollers",
   ISBN: 978-1469998749, Jonathan Valvano, copyright (c) 2014

 Copyright 2015 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */
// ******* Possible Hardware I/O connections*******************
// Slide pot pin 1 connected to ground
// Slide pot pin 2 connected to PE2/AIN1
// Slide pot pin 3 connected to +3.3V 
// fire button connected to PE0
// special weapon fire button connected to PE1
// 8*R resistor DAC bit 0 on PB0 (least significant bit)
// 4*R resistor DAC bit 1 on PB1
// 2*R resistor DAC bit 2 on PB2
// 1*R resistor DAC bit 3 on PB3 (most significant bit)
// LED on PB4
// LED on PB5

// Backlight (pin 10) connected to +3.3 V
// MISO (pin 9) unconnected
// SCK (pin 8) connected to PA2 (SSI0Clk)
// MOSI (pin 7) connected to PA5 (SSI0Tx)
// TFT_CS (pin 6) connected to PA3 (SSI0Fss)
// CARD_CS (pin 5) unconnected
// Data/Command (pin 4) connected to PA6 (GPIO), high for data, low for command
// RESET (pin 3) connected to PA7 (GPIO)
// VCC (pin 2) connected to +3.3 V
// Gnd (pin 1) connected to ground

#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "ST7735.h"
#include "Random.h"
#include "TExaS.h"
#include "DAC.h"
#include "ADC.h"
#include "Edgeinterrupt.h"
#include "Timer.h"
#include "Print.h"
#include "Sound.h"
#include "BitMaps.h"
#include "Trig.h"
#include "SoundEffects.h"
#include "SpaceInvaders.h"

#define	numPlatforms 10
#define	numEnemies	8
#define	numEnemyProj	6
#define	numPlayerProj	6
#define numPowerUps		3
#define enemyProjTime 32

void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts
void Delay100ms(uint32_t count); // time delay in 0.1 seconds
void ADC_Init(void);

extern uint8_t JumpMail;
extern uint8_t booboo[20480];


uint8_t gamestate = start;
uint8_t titlesemaphore = 1;
uint32_t scoretimer = 5400;
uint8_t semaphore = 0;
uint8_t deadflag = 0;
uint8_t loseflag = 0;
uint8_t winflag = 0;
uint8_t powerupcounter = 0;
uint8_t playerLives = 3;
uint8_t grohasDestroyed = 0;
uint8_t rohasDestroyed = 0;
int32_t camera = 0;
uint32_t poweruptimer = 25;
uint32_t ADCMail;
uint8_t ADCFlag;
uint32_t waveidx;
uint16_t soundbytelength;
uint8_t soundbyteselect;
uint8_t pPidx;
uint8_t ePidx;
uint8_t xPlatform;
uint8_t xPlatformFlag;
uint32_t powerupselect;
uint8_t musicenable;


platformType platformInstance[numPlatforms] = {
	{14, 0, 6, 50, active},
	{14, 100, 6, 50, active},
	{14, 200, 6, 50, active},
	{14, 300, 6, 50, active},
	{14, 400, 6, 50, active},
	{14, 500, 6, 50, active},
	{14, 600, 6, 50, active},
	{14, 700, 6, 50, active},
	{14, 800, 6, 50, active},
	{14, 900, 6, 50, active},

};
projectileType playerProjectileInstance[numPlayerProj] = {
	{0, 0, 0, punji1, 5, 16, inactive, 0},
	{0, 0, 0, punji1, 5, 16, inactive, 0},
	{0, 0, 0, punji1, 5, 16, inactive, 0},
	{0, 0, 0, punji1, 5, 16, inactive, 0},
	{0, 0, 0, punji1, 5, 16, inactive, 0},
	{0, 0, 0, punji1, 5, 16, inactive, 0}
};
projectileType enemyProjectileInstance[numEnemyProj] = {
	{0, 0, 0, punji2, 5, 16, inactive, 0},
	{0, 0, 0, punji2, 5, 16, inactive, 0},
	{0, 0, 0, punji2, 5, 16, inactive, 0},
	{0, 0, 0, punji2, 5, 16, inactive, 0},
	{0, 0, 0, punji2, 5, 16, inactive, 0},
	{0, 0, 0, punji2, 5, 16, inactive, 0}
};

powerupType powerupInstance[numPowerUps] = {
	{20, 10, BOLT, 19, 19, inactive, boltpu, 8228, 0},
	{20, 50, VOVO, 19, 19, inactive, vovopu, 19079, 0},
	{20, 100, SH, 19, 19, inactive, shpu, 10192, 0}
};		
	
playerSpriteType playerSpriteInstance = {30, 14, 0, 4, 2, 1, right, gong, 30, 21, active, 0, 0, vovopu};
enemySpriteType enemySpriteInstance[numEnemies] = {
	{20, 120, 0, 2, 2, 1, right, Bo, 41, 27, active, 0, 0, 0, normal, 1},
	{20, 220, 0, -2, 2, 1, left, Bo, 41, 27, active, 0, 0, 0, normal, 2},
	{20, 320, 0, -2, 2, 1, left, Bo, 41, 27, active, 0, 0, 0, normal, 3},
	{20, 420, 0, -2, 2, 1, left, Bo, 41, 27, active, 0, 0, 0, normal, 4},
	{20, 520, 0, -2, 2, 1, left, Bo, 41, 27, active, 0, 0, 0, normal, 5},
	{20, 620, 0, -2, 2, 1, left, Bo, 41, 27, active, 0, 0, 0, normal, 6},
	{20, 720, 0, -2, 2, 1, left, Bo, 41, 27, active, 0, 0, 0, normal, 7},
	{20, 820, 0, -2, 2, 1, left, Bo, 41, 27, active, 0, 0, 0, normal, 8}
};



int main(void){
  TExaS_Init();  // set system clock to 80 MHz
  ST7735_InitR(INITR_REDTAB);
	ADC_Init();
	DAC_Init();
	EdgeTrigger_Init();
	SysTick_Init(10000000);
	Timer2_Init(&InterruptTask2, 7272); // initliaze Timer2A so that it interrupts at 11 kHz
  Output_Init();
	Random_Init(NVIC_ST_CURRENT_R);

	while(1) {
		
		if(gamestate == start) {
			musicenable = 0;
			gameInit();
			titleScreen();
		}
		if(gamestate == play) {
			musicenable = 1;
			ST7735_WriteToBooboo(0, 0, background, 128, 160, 0);
			gameEngine();
		}
		if(gamestate == win) {
			winScreen();
		}
		if(gamestate == lose) {
			loseScreen();
		}
	}
}

void clearBooboo(void) {
	for(uint16_t i = 0; i < 20480; i++) {
		booboo[i] = 0x00;
	}
}
void gameInit(void) {
	scoretimer = 5400;
	semaphore = 0;
	deadflag = 0;
	loseflag = 0;
	winflag = 0;
	powerupcounter = 0;
	playerLives = 3;
	grohasDestroyed = 0;
	rohasDestroyed = 0;
	camera = 0;
	
	playerInit(30, camera, 0, 4, 2, 1, right, gong, 30, 21, active, 0, 0, shpu);
	enemyInit(20, 120, 0, 2, 2, 1, right, Bo, 41, 27, active, 0, 0, 0, normal, 1, 0);
	enemyInit(20, 220, 0, -2, 2, 1, left, Bo, 41, 27, active, 0, 0, 0, normal, 2, 1);
	enemyInit(20, 320, 0, 2, 2, 1, right, Bo, 41, 27, active, 0, 0, 0, shooter, 3, 2);
	enemyInit(20, 420, 0, -2, 2, 1, left, Bo, 41, 27, active, 0, 0, 0, normal, 4, 3);
	enemyInit(20, 520, 0, 2, 2, 1, right, Bo, 41, 27, active, 0, 0, 0, normal, 5, 4);
	enemyInit(20, 620, 0, -2, 2, 1, left, Bo, 41, 27, active, 0, 0, 0, shooter, 6, 5);
	enemyInit(20, 720, 0, 4, 2, 1, right, Bo, 41, 27, active, 0, 0, 0, shooter, 7, 6);
	enemyInit(20, 820, 0, -4, 2, 1, left, Bo, 41, 27, active, 0, 0, 0, shooter, 8, 7);
}

void nextLife(void) {
	ADCMail = 0;
	ADCFlag = 0;
	JumpMail = 0;
	waveidx = 0;
	pPidx = 0;
	ePidx = 0;
	xPlatform = 0;
	xPlatformFlag = 0;
	
	checkpointInit(checkpointSet());
	
	for (uint8_t i = 0; i < numEnemyProj; i++) {
		enemyprojectileInit(0, 0, 0, punji2, 5, 16, inactive, 0, i);
	}
	for (uint8_t i = 0; i < numPlayerProj; i++) {
		enemyprojectileInit(0, 0, 0, punji2, 5, 16, inactive, 0, i);
	}
}

void gameEngine(void) {
	if(semaphore == 1) {
		enemyFireProjectile();
		punjiDetection(&(*enemySpriteInstance), &(*playerProjectileInstance));
		punjiDetection2(&playerSpriteInstance, &(*enemyProjectileInstance));
		powerupDetection(&playerSpriteInstance, &(*powerupInstance));
		enemyDetection(&playerSpriteInstance, &(*enemySpriteInstance));
		movePlayer(&playerSpriteInstance);
		cameraUpdate();
		moveEnemy(&(*enemySpriteInstance), &(*platformInstance));
		movePlayerProjectile(&(*playerProjectileInstance));
		moveEnemyProjectile(&(*enemyProjectileInstance));
		movePlatform(&(*platformInstance));
		movePowerUp((&(*powerupInstance)));
		gongruyenLives();
		ST7735_Dump(0, 159, booboo, 128, 160);
		
		if (poweruptimer == 0) {
			playerSpriteInstance.powerup = normal;
		} else {
			poweruptimer--;
		}
		
		if(playerLives == 0) {
			gamestate = lose;
			loseflag = 1;
		} else if (deadflag == 1) {
			nextLife();
			deadflag = 0;
		}
		
		scoretimer--;
		if (scoretimer == 0) {
			gamestate = lose;
		}
		
		semaphore = 0;
	}
}

void winScreen (void){
	if(semaphore == 1 && winflag == 1){	
		ST7735_WriteToBooboo(0, 0, dohaday, 128, 160, 0);
		ST7735_Dump(0, 159, booboo, 128, 160);
		scoreCalculateWin();
		winflag = 0;
	}
}

void loseScreen (void){
	if(semaphore == 1 && loseflag == 1){	
		ST7735_WriteToBooboo(0, 0, dohanight, 128, 160, 0);
		ST7735_Dump(0, 159, booboo, 128, 160);
		scoreCalculateLose();
		loseflag = 0;
	}
}

void scoreCalculateLose(void) {
	ST7735_SetRotation(1);
	ST7735_SetCursor(0,0);
	ST7735_OutString("Nice try, YOU LOSE!\r");
	ST7735_OutString("...Let's go to DOHA.\r\r");
	ST7735_OutString("Grohas Destroyed: x");
  LCD_OutDec(grohasDestroyed); ST7735_OutString("\r"); 
	ST7735_OutString("Rohas Destroyed: x");
  LCD_OutDec(rohasDestroyed); 
};

void scoreCalculateWin(void) {
	uint32_t myscore = playerLives * 100 + grohasDestroyed * 10 + rohasDestroyed * 30 + 5400 - scoretimer;
	ST7735_SetRotation(1);
	ST7735_SetCursor(0,0);
	ST7735_OutString("Whoa-ha, YOU WIN!\r");
	ST7735_OutString("...Let's go to DOHA.\r\r");
	ST7735_OutString("Grohas Destroyed: x");
  LCD_OutDec(grohasDestroyed); ST7735_OutString("\r"); 
	ST7735_OutString("Rohas Destroyed: x");
  LCD_OutDec(rohasDestroyed); ST7735_OutString("\r"); 
	ST7735_OutString("Score: "); LCD_OutDec(myscore); ST7735_OutString("points");
}

void gongruyenLives(void) {
	for(uint8_t i = 0; i < playerLives; i++) {
		ST7735_BackToBooboo(110, i*16, 16, 16);
		ST7735_WriteToBooboo(110, i*16, lives, 16, 16, 0);
	}
}

void movePlatform(struct platformTag *point) {
	for(uint8_t i = 0; i < numPlatforms; i++) {
		if ((*(point+i)).status == active) {
			ST7735_FillRectBooboo((*(point+i)).xpos, (*(point+i)).ypos - camera, (*(point+i)).wd, (*(point+i)).ht, ST7735_RED);	
		}
	}
}

void movePlayerProjectile(struct projectileTag *point) {
	for (uint8_t i = 0; i < numPlayerProj; i++) {
		if ((*(point+i)).backgroundflag == 1) {
			ST7735_BackToBooboo((*(point+i)).xpos, (*(point+i)).ypos - camera, 5, 16);
			(*(point+i)).backgroundflag = 0;
		}
		if ((*(point+i)).status == active) {
			ST7735_BackToBooboo((*(point+i)).xpos, (*(point+i)).ypos - camera, 5, 16);
			(*(point+i)).ypos += (*(point+i)).dy;
			ST7735_WriteToBooboo((*(point+i)).xpos, (*(point+i)).ypos - camera, (*(point+i)).pixeldata, 5, 16, 0);
			if ((*(point+i)).ypos > 160 + camera || (*(point+i)).ypos < 0) {
				(*(point+i)).status = inactive;
			}
		}
	}
}
void moveEnemyProjectile(struct projectileTag *point) {
	for (uint8_t i = 0; i < numEnemyProj; i++) {
		if ((*(point+i)).backgroundflag == 1) {
			ST7735_BackToBooboo((*(point+i)).xpos, (*(point+i)).ypos - camera, 5, 16);
			(*(point+i)).backgroundflag = 0;
		}
		if ((*(point+i)).status == active) {
			ST7735_BackToBooboo((*(point+i)).xpos, (*(point+i)).ypos - camera, 5, 16);
			(*(point+i)).ypos += (*(point+i)).dy;
			ST7735_WriteToBooboo((*(point+i)).xpos, (*(point+i)).ypos - camera, (*(point+i)).pixeldata, 5, 16, 0);
			if ((*(point+i)).ypos > 160 + camera || (*(point+i)).ypos < 0) {
				(*(point+i)).status = inactive;
			}
		}
	}
}
void movePowerUp(struct powerupTag *point) {
	for(uint8_t i = 0; i < numPowerUps; i++){
		if ((*(point+i)).backgroundflag == 1) {
			ST7735_BackToBooboo((*(point+i)).xpos, (*(point+i)).ypos - camera, 19, 19);
			(*(point+i)).backgroundflag = 0;
		}
		if ((*(point+i)).status == active) {
			ST7735_BackToBooboo((*(point+i)).xpos, (*(point+i)).ypos - camera, 19, 19);
			ST7735_WriteToBooboo((*(point+i)).xpos, (*(point+i)).ypos - camera, (*(point+i)).pixeldata, 19, 19, 0);
		}
	}
}

void PlayerControl(struct playerSpriteTag *point) {
	int32_t mail = ADCMail;
	if(mail < 2000) {
		(*point).facing = left;
		mail = (mail - 2048) / 400;
	} else if (mail > 2096) {
		(*point).facing = right;
		mail = (mail - 2048) / 400;
	}
	(*point).dy = mail;
}

void movePlayer(struct playerSpriteTag *point) {
	ST7735_BackToBooboo((*point).xpos, (*point).ypos - camera, (*point).wd, (*point).ht);
	
	PlayerControl(point);
	
	if((*point).ypos - camera < 0){
		(*point).dy = 0;
		(*point).ypos = camera;
	}
	
	if (platformDetection(point, platformInstance) == nofall) {
		(*point).jumping = 0;
	} else {
		(*point).jumping = 1;
	}

	if((*point).jumping == 0) {
		if(JumpMail == 1) {
			if((*point).powerup == boltpu) {
				(*point).dx = 20;
			} else {
				(*point).dx = 14;
			}		
		} else {
			(*point).dx = 0;
		}
	} else {
		(*point).dx -= 2;
	}	
	JumpMail = 0;
	
	if (xPlatformFlag == 1) {
		(*point).xpos = xPlatform;
		xPlatformFlag = 0;
	} else {
		(*point).xpos += (*point).dx;
	}
	(*point).ypos += (*point).dy;
	if((*point).status == active) {
		ST7735_WriteToBooboo((*point).xpos, (*point).ypos - camera, (*point).pixeldata+((*point).currentframe+(*point).facing*(*point).animationframes)*(*point).wd*(*point).ht, (*point).wd, (*point).ht, (*point).powerup);
	}
	(*point).currentframe = ((*point).currentframe + 1)%((*point).animationframes);
	if((*point).ypos < (*point).ht) {
		(*point).dy = -(*point).dy;
	}
	if((*point).xpos + (*point).wd < 0) {
		playerLives--;
		deadflag = 1;
	}
	if((*point).ypos > 950) {
		gamestate = win;
		winflag = 1;
	}
}

void enemyDies(struct enemySpriteTag *enpoint, uint8_t i) { 
	powerupselect = Random();
	uint8_t powerupdrop;
	if (powerupselect >= 85 && powerupselect < 128) {
		powerupdrop = shpu;
		powerupInit((*(enpoint+i)).xpos, (*(enpoint+i)).ypos, SH, 19, 19, active, powerupdrop, 10192, 0, powerupcounter);
	} else if (powerupselect >= 42 && powerupselect < 85 ) {
		powerupdrop = vovopu;
		powerupInit((*(enpoint+i)).xpos, (*(enpoint+i)).ypos, VOVO, 19, 19, active, powerupdrop, 19079, 0, powerupcounter);
	} else if (powerupselect < 42) {
		powerupdrop = boltpu;
		powerupInit((*(enpoint+i)).xpos, (*(enpoint+i)).ypos, BOLT, 19, 19, active, powerupdrop, 8228, 0, powerupcounter);
	}
	if (powerupselect < 128) {
		powerupcounter = (powerupcounter+1)%numPowerUps;
	}
	if((*(enpoint+i)).enemytype == shooter) {
		rohasDestroyed++;
	} else if ((*(enpoint+i)).enemytype == normal) {
		grohasDestroyed++;
	}
}

void enemyDetection(struct playerSpriteTag *point, struct enemySpriteTag *enpoint) {
	for(uint8_t i = 0; i < numEnemies; i++) {
		if ((*(enpoint+i)).status == active && (*point).status == active) {
			if (((*point).ypos >= (*(enpoint+i)).ypos && (*point).ypos <= (*(enpoint+i)).ypos + (*(enpoint+i)).ht) ||
				(((*point).ypos + (*point).ht >= (*(enpoint+i)).ypos) && ((*point).ypos + (*point).ht <= (*(enpoint+i)).ypos + (*(enpoint+i)).ht))) {
				if (((*point).xpos >= (*(enpoint+i)).xpos + 25) && ((*point).xpos <= (*(enpoint+i)).xpos + (*(enpoint+i)).wd)) {
					(*(enpoint+i)).status = inactive;
					enemyDies(&(*enpoint), i);
				} else if (((*point).xpos >= (*(enpoint+i)).xpos) && ((*point).xpos < (*(enpoint+i)).xpos + 25)) {
					if ((*point).powerup != vovopu && deadflag != 1) {
						playerLives--;
						deadflag = 1;
					} else {
						(*(enpoint+i)).status = inactive;
						enemyDies(&(*enpoint), i);
					}
				}	
			}
		}
	}
}	

uint8_t platformDetection(struct playerSpriteTag *point, struct platformTag *pltpoint) {
	for(uint8_t i = 0; i < numPlatforms; i++) {
		if ((*(pltpoint+i)).status == active) {
			if((*point).ypos + (*point).ht >= (*(pltpoint+i)).ypos && (*point).ypos <= (*(pltpoint+i)).ypos + (*(pltpoint+i)).ht) {
				if((*(pltpoint+i)).xpos + (*(pltpoint+i)).wd <= (*point).xpos && (*(pltpoint+i)).xpos + (*(pltpoint+i)).wd >= (*point).xpos + (*point).dx) {
					xPlatform = (*(pltpoint+i)).xpos+(*(pltpoint+i)).wd;
					xPlatformFlag = 1;	
					return nofall;
				}
			}
		}	
	}
	return fall;
}

void punjiDetection(struct enemySpriteTag *point, struct projectileTag *projpoint)	{
	for(uint8_t i = 0; i < numPlayerProj; i++){
		for(uint8_t j = 0; j < numEnemies; j++){
			if((*(projpoint+i)).status == active && (*(point+j)).status == active){
				if((((*(projpoint+i)).ypos + (*(projpoint+i)).ht >= (*(point+j)).ypos	&& (*(projpoint+i)).ypos + (*(projpoint+i)).ht <= (*(point+j)).ypos + (*(point+j)).ht) ||
					((*(projpoint+i)).ypos >= (*(point+j)).ypos	&& (*(projpoint+i)).ypos <= (*(point+j)).ypos + (*(point+j)).ht)) &&
					(((*(projpoint+i)).xpos + (*(projpoint+i)).wd >= (*(point+j)).xpos	&& (*(projpoint+i)).xpos + (*(projpoint+i)).wd <= (*(point+j)).xpos + (*(point+j)).wd) ||
					((*(projpoint+i)).xpos >= (*(point+j)).xpos	&& (*(projpoint+i)).xpos <= (*(point+j)).xpos + (*(point+j)).wd))) {
					
					(*(projpoint+i)).status = inactive;
					(*(projpoint+i)).backgroundflag = 1;
					(*(point+j)).status = inactive;
					(*(point+j)).backgroundflag = 1;
					enemyDies(&(*point), i);
				}
			}
		}
	}
}

void punjiDetection2(struct playerSpriteTag *point, struct projectileTag *projpoint)	{
	for(uint8_t i = 0; i < numEnemyProj; i++){
		if((*(projpoint+i)).status == active && (*point).status == active){
			if((((*(projpoint+i)).ypos + (*(projpoint+i)).ht >= (*point).ypos	&& (*(projpoint+i)).ypos + (*(projpoint+i)).ht <= (*point).ypos + (*point).ht) ||
				((*(projpoint+i)).ypos >= (*point).ypos	&& (*(projpoint+i)).ypos <= (*point).ypos + (*point).ht)) &&
				(((*(projpoint+i)).xpos + (*(projpoint+i)).wd >= (*point).xpos	&& (*(projpoint+i)).xpos + (*(projpoint+i)).wd <= (*point).xpos + (*point).wd) ||
				((*(projpoint+i)).xpos >= (*point).xpos	&& (*(projpoint+i)).xpos <= (*point).xpos + (*point).wd))) {
				
				(*(projpoint+i)).status = inactive;
				(*(projpoint+i)).backgroundflag = 1;
				(*point).status = inactive;
				(*point).backgroundflag = 1;
				playerLives--;
				deadflag = 1;
			}
		}
	}
}

void powerupDetection(struct playerSpriteTag *point, struct powerupTag *pupoint){
		for(uint8_t i = 0; i < numPowerUps; i++){
			if((*(pupoint+i)).status == active && (*point).status == active){
				if((((*(pupoint+i)).ypos + (*(pupoint+i)).ht >= (*point).ypos	&& (*(pupoint+i)).ypos + (*(pupoint+i)).ht <= (*point).ypos + (*point).ht) ||
					((*(pupoint+i)).ypos >= (*point).ypos	&& (*(pupoint+i)).ypos <= (*point).ypos + (*point).ht)) &&
					(((*(pupoint+i)).xpos + (*(pupoint+i)).wd >= (*point).xpos	&& (*(pupoint+i)).xpos + (*(pupoint+i)).wd <= (*point).xpos + (*point).wd) ||
					((*(pupoint+i)).xpos >= (*point).xpos	&& (*(pupoint+i)).xpos <= (*point).xpos + (*point).wd))) {
					
					(*point).powerup = (*(pupoint+i)).power;
					poweruptimer = 100;	
					soundbyteselect = (*(pupoint+i)).power;
					soundbytelength = (*(pupoint+i)).soundlength;
					waveidx = 0;
					(*(pupoint+i)).status = inactive;
					(*(pupoint+i)).backgroundflag = 1;
				}
			}
	}
}	

void moveEnemy(struct enemySpriteTag *point, struct platformTag *pltpoint) {
	for (uint8_t i = 0; i < numEnemies; i++) {
		if ((*(point+i)).backgroundflag == 1) {
			ST7735_BackToBooboo((*(point+i)).xpos, (*(point+i)).ypos - camera, (*(point+i)).wd, (*(point+i)).ht);
			(*(point+i)).backgroundflag = 0;
		}
		if ((*(point+i)).status == active) {
			ST7735_BackToBooboo((*(point+i)).xpos, (*(point+i)).ypos - camera, (*(point+i)).wd, (*(point+i)).ht);
			
			(*(point+i)).xpos += (*(point+i)).dx;
			(*(point+i)).ypos += (*(point+i)).dy;
			
			ST7735_WriteToBooboo((*(point+i)).xpos, (*(point+i)).ypos - camera, (*(point+i)).pixeldata+((*(point+i)).currentframe+(*(point+i)).facing*(*(point+i)).animationframes)*(*(point+i)).wd*(*(point+i)).ht, (*(point+i)).wd, (*(point+i)).ht, (*(point+i)).enemytype);
			(*(point+i)).currentframe = ((*(point+i)).currentframe + 1)%((*(point+i)).animationframes);
			if((*(point+i)).ypos < (*(pltpoint+((*(point+i)).platform))).ypos || (*(point+i)).ypos + (*(point+i)).ht > (*(pltpoint+((*(point+i)).platform))).ypos + (*(pltpoint+((*(point+i)).platform))).ht){
				(*(point+i)).dy = -(*(point+i)).dy;
				(*(point+i)).facing^=1;
			}
		}
		if((*(enemySpriteInstance+i)).projspacetimer != 0) {
			(*(enemySpriteInstance+i)).projspacetimer = ((*(enemySpriteInstance+i)).projspacetimer + 1)%enemyProjTime;
		}
	}
}

void playerFireProjectile(void) {
	if (playerSpriteInstance.powerup == shpu) {
		playerProjectileInstance[pPidx].xpos = playerSpriteInstance.xpos+playerSpriteInstance.wd/2;
		if(playerSpriteInstance.facing == right) {
			playerProjectileInstance[pPidx].ypos = playerSpriteInstance.ypos+playerSpriteInstance.ht;	
			playerProjectileInstance[pPidx].dy = 8;
		} else {
			playerProjectileInstance[pPidx].ypos = playerSpriteInstance.ypos;	
			playerProjectileInstance[pPidx].dy = -8;
		}
		
		playerProjectileInstance[pPidx].status = active;
		pPidx = (pPidx+1)%numPlayerProj;
		soundbyteselect = jalilisound;
		soundbytelength = 4080;
		waveidx = 0;
	}
}
void enemyFireProjectile(void) {
	for(uint8_t i = 0; i < numEnemies; i++){
		if((*(enemySpriteInstance+i)).enemytype == shooter && (*(enemySpriteInstance+i)).status == active && (*(enemySpriteInstance+i)).projspacetimer == 0) {
			if((playerSpriteInstance.ypos < (*(enemySpriteInstance+i)).ypos) && (*(enemySpriteInstance+i)).facing == left) {
				enemyProjectileInstance[ePidx].ypos = (*(enemySpriteInstance+i)).ypos;	
				enemyProjectileInstance[ePidx].dy = -8;
				enemyProjectileInstance[ePidx].xpos = (*(enemySpriteInstance+i)).xpos+(*(enemySpriteInstance+i)).wd/2;
				enemyProjectileInstance[ePidx].status = active;
				ePidx = (ePidx+1)%numEnemyProj;
				(*(enemySpriteInstance+i)).projspacetimer = 1;
			} else if ((playerSpriteInstance.ypos > (*(enemySpriteInstance+i)).ypos+(*(enemySpriteInstance+i)).ht) && (*(enemySpriteInstance+i)).facing == right) {
				enemyProjectileInstance[ePidx].ypos = (*(enemySpriteInstance+i)).ypos+(*(enemySpriteInstance+i)).ht;	
				enemyProjectileInstance[ePidx].dy = 8;
				enemyProjectileInstance[ePidx].xpos = (*(enemySpriteInstance+i)).xpos+(*(enemySpriteInstance+i)).wd/2;
				enemyProjectileInstance[ePidx].status = active;
				ePidx = (ePidx+1)%numEnemyProj;
				(*(enemySpriteInstance+i)).projspacetimer = 1;
			}	
		}
	}
}
	
void cameraUpdate(void) {
	if((playerSpriteInstance.ypos > camera + 80) && (playerSpriteInstance.dy > 0)) {
		 camera += playerSpriteInstance.dy;
	}
}

uint8_t checkpointSet(void) {
	for (uint8_t i = 10; i >= 1; i--) {
		if (playerSpriteInstance.ypos >= 100*(i)) {
			return (i - 1);
		}
	}
	return 0;
}

void checkpointInit(uint8_t checkpointnum) {
	camera = 100*checkpointnum;
	playerInit(30, camera, 0, 4, 2, 1, right, gong, 30, 21, active, 0, 0, normal);
}


void titleScreen(void) {
	if(titlesemaphore == 1) {
		ST7735_SetCursor(0, 0);
		ST7735_SetRotation(0);
		ST7735_FillScreen(0x0000);
		clearBooboo();
		ST7735_WriteToBooboo(63, 12, GongruyenTitle, 22, 136, 0);
		ST7735_Dump(0, 159, booboo, 128, 160);
		ST7735_WriteToBooboo(0, 0, background, 128, 160, 0);
		
		soundbyteselect = gongruyensound;
		soundbytelength = 18636;
		waveidx = 0;
		
		titlesemaphore = 0;
	}
}
void playerInit(int16_t xpos, int16_t ypos, int8_t dx, int8_t dy, uint8_t animationframes, uint8_t currentframe, uint8_t facing, const unsigned short *pixeldata,
	uint16_t wd, uint16_t ht, uint8_t status, uint8_t jumping, uint8_t backgroundflag, uint8_t powerup) {
	playerSpriteInstance.xpos = xpos;
	playerSpriteInstance.ypos = ypos;
	playerSpriteInstance.dx = dx;
	playerSpriteInstance.dy = dy;
	playerSpriteInstance.animationframes = animationframes;
	playerSpriteInstance.currentframe = currentframe;
	playerSpriteInstance.facing = facing;
	playerSpriteInstance.pixeldata = pixeldata;
	playerSpriteInstance.wd = wd;
	playerSpriteInstance.ht = ht;
	playerSpriteInstance.status = status;
	playerSpriteInstance.jumping = jumping;
	playerSpriteInstance.backgroundflag = backgroundflag;
	playerSpriteInstance.powerup = powerup;
}
void enemyInit(int16_t xpos, int16_t ypos, int8_t dx, int8_t dy, uint8_t animationframes, uint8_t currentframe, uint8_t facing, const unsigned short *pixeldata,
	uint16_t wd, uint16_t ht, uint8_t status, uint8_t jumping, uint8_t backgroundflag, uint8_t projspacetimer, uint8_t enemytype, uint8_t platform, uint8_t index) {
	
	enemySpriteInstance[index].xpos = xpos;
	enemySpriteInstance[index].ypos = ypos;
	enemySpriteInstance[index].dx = dx;
	enemySpriteInstance[index].dy = dy;
	enemySpriteInstance[index].animationframes = animationframes;
	enemySpriteInstance[index].currentframe = currentframe;
	enemySpriteInstance[index].facing = facing;
	enemySpriteInstance[index].pixeldata = pixeldata;
	enemySpriteInstance[index].wd = wd;
	enemySpriteInstance[index].ht = ht;
	enemySpriteInstance[index].status = status;
	enemySpriteInstance[index].jumping = jumping;
	enemySpriteInstance[index].backgroundflag = backgroundflag;
	enemySpriteInstance[index].projspacetimer = projspacetimer;
	enemySpriteInstance[index].enemytype = enemytype;
	enemySpriteInstance[index].platform = platform;
}

void platformInit(uint32_t xpos, uint32_t ypos, uint8_t wd, uint8_t ht, uint8_t status, uint8_t index) {
	platformInstance[index].xpos = xpos;
	platformInstance[index].ypos = ypos;
	platformInstance[index].wd = wd;
	platformInstance[index].ht = ht;
	platformInstance[index].status = status;
}
void enemyprojectileInit(int16_t xpos, int16_t ypos, int8_t dy, const unsigned short *pixeldata, uint8_t wd, uint8_t ht, uint8_t status, uint8_t backgroundflag, uint8_t index) {
	enemyProjectileInstance[index].xpos = xpos;
	enemyProjectileInstance[index].ypos = ypos;
	enemyProjectileInstance[index].dy = dy;
	enemyProjectileInstance[index].pixeldata = pixeldata;
	enemyProjectileInstance[index].wd = wd;
	enemyProjectileInstance[index].ht = ht;
	enemyProjectileInstance[index].status = status;
	enemyProjectileInstance[index].backgroundflag = backgroundflag;
}
void playerprojectileInit(int16_t xpos, int16_t ypos, int8_t dy, const unsigned short *pixeldata, uint8_t wd, uint8_t ht, uint8_t status, uint8_t backgroundflag, uint8_t index) {
	playerProjectileInstance[index].xpos = xpos;
	playerProjectileInstance[index].ypos = ypos;
	playerProjectileInstance[index].dy = dy;
	playerProjectileInstance[index].pixeldata = pixeldata;
	playerProjectileInstance[index].wd = wd;
	playerProjectileInstance[index].ht = ht;
	playerProjectileInstance[index].status = status;
	playerProjectileInstance[index].backgroundflag = backgroundflag;
}

void powerupInit(int16_t xpos, int16_t ypos, const unsigned short *pixeldata, uint8_t wd,	uint8_t ht, uint8_t status, uint8_t power, uint16_t soundlength, uint8_t backgroundflag, uint8_t index) {
	powerupInstance[index].xpos = xpos;
	powerupInstance[index].ypos = ypos;
	powerupInstance[index].pixeldata = pixeldata;
	powerupInstance[index].wd = wd;
	powerupInstance[index].ht = ht;
	powerupInstance[index].status = status;
	powerupInstance[index].power = power;
	powerupInstance[index].soundlength = soundlength;
	powerupInstance[index].backgroundflag = backgroundflag;
}


